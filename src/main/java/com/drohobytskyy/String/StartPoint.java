package com.drohobytskyy.String;

import com.drohobytskyy.String.Controller.Controller;
import com.drohobytskyy.String.Model.MyFileReader;
import com.drohobytskyy.String.View.ConsoleView;
import java.io.IOException;

public class StartPoint {
    public static void main(String[] args) {
        try {
            Controller controller = new Controller(new ConsoleView(), new MyFileReader());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
