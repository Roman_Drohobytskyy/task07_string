package com.drohobytskyy.String.Model;

import com.drohobytskyy.String.View.ConsoleView;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MyFileReader {
    private final static String FILE_NAME = "src\\main\\resources\\TextFile.txt";
    private StringBuilder textFromFile;
    public ConsoleView view;
    public int length;
    public List<Sentence> sentencesList;
    String stringTextFromFile;

    public void readerExecute() {
        textFromFile = new StringBuilder();
        readFromFileToString();
        view.logger.warn(textFromFile);
        removeUnnecessaryElements();
        split(stringTextFromFile);
    }

    public static void main(String[] args) throws IOException {
        MyFileReader fr = new MyFileReader();
        fr.view = new ConsoleView();
        fr.readerExecute();
        fr.printAllSentences(fr.sentencesList);
        //fr.countOfOccurrencesOfWordInArray("молодець");
        //fr.countOfOccurrencesOfWordFromList();
        fr.findSentenceWithMaxCountOfEqualWords();
    }

    public void findSentenceWithMaxCountOfEqualWords() {
        Arrays.stream(sentencesToArray(sentencesList))
                .sorted((array1, array2) -> countOfEqualWords(array1) > countOfEqualWords(array2) ? -1 : 1)
        .limit(1)
        .forEach(array -> view.logger.info("Sentence with the biggest quantity of equal words: "
                + arrayToString(array) + "   count of repetitions: " + countOfEqualWords(array)));
    }

    private int countOfEqualWords(String[] array) {
        List<String> list = Arrays.asList(array);
        Set<String> uniqueWords = new HashSet<String>(list);
        return list.size() - uniqueWords.size();
    }

    private void removeUnnecessaryElements() {
        Pattern CLEAR_PATTERN = Pattern.compile("[\\s]+|[\\t]+|[\\n]+");
        stringTextFromFile = CLEAR_PATTERN.matcher(textFromFile).replaceAll(" ").trim();
    }

    private void arrayToListOfSentences(String[][] words) {
        sentencesList = new ArrayList<>();
        for (int s = 0; s < words.length; s++) {
            Sentence sentence = new Sentence();
            length++;
            for (int w = 0; w < words[s].length; w++) {
                sentence.getWords().add(new Word(words[s][w]));
            }
            sentencesList.add(sentence);
        }
    }

    private void readFromFileToString() {
        try (FileReader reader = new FileReader(FILE_NAME)) {
            int c;
            while ((c = reader.read()) != -1) {
                textFromFile.append((char) c);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void split(String text) {
        String[] sentences = text.split("[.!?]\\s*");
        String[][] words = new String[sentences.length][];
        for (int i = 0; i < sentences.length; ++i) {
            words[i] = sentences[i].split("[\\p{Punct}\\s]+");
        }
        arrayToListOfSentences(words);
    }

    private int checkFirstConsonant(String s) {
        Pattern p = Pattern.compile("[^aeiouаоуеиіАОУЕИІ]{1}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(s);
        if (matcher.find()) {
            return matcher.start();
        } else {
            return -1;
        }
    }

    private boolean checkIfStartsFromVowel(String s) {
        Pattern p = Pattern.compile("^[aeiouаоуеиіАОУЕИІ]+", Pattern.CASE_INSENSITIVE);
        if (p.matcher(s).find()) {
            return true;
        }
        return false;
    }

    private double checkVowels(String s) {
        return (s.length() - s.toLowerCase().replaceAll(
                "a|e|i|o|u|A|E|I|O|U|а|о|у|е|и|і|А|О|У|Е|И|І|", "").length()) / (double) s.length();
    }

    private void printSentence(Sentence sentence) {
        view.logger.info(sentence);

    }

    public void printAllSentences(List<Sentence> sentences) {
        view.logger.warn("-------------------------------------------------------------------------------------"
                + "-------------------------------------------------");

        for (Sentence sentence : sentences) {
            printSentence(sentence);
        }
        view.logger.warn("-------------------------------------------------------------------------------------"
                + "-------------------------------------------------");
    }

    private String[][] sentencesToArray(List<Sentence> sentencesList) {
        String[][] words = new String[sentencesList.size()][];
        String[] sentence;
        for (int i = 0; i < sentencesList.size(); i++) {
            sentence = new String[sentencesList.get(i).getWords().size()];
            for (int j = 0; j < sentencesList.get(i).getWords().size(); j++) {
                sentence[j] = sentencesList.get(i).getWords().get(j).getWord();
            }
            words[i] = sentence;
        }
        return words;
    }

    public void printArray(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            StringBuilder sentence = new StringBuilder();
            for (int j = 0; j < array[i].length; j++) {
                if (j == array[i].length - 1) {
                    sentence.append(array[i][j]);
                } else {
                    sentence.append(array[i][j] + ", ");
                }
            }
            view.logger.info(sentence);
        }
    }

    public void findUniqueWordInTextFromFirstSentence() {
        boolean count = false;
        sentencesList.get(0).getWords().stream().filter(word ->
                Arrays.stream(sentencesToArray(sentencesList))
                        .skip(1)
                        .flatMap(Arrays::stream)
                        .noneMatch(word.getWord()::equals))
                .forEach(word -> view.logger.info("Unique word: " + word));
    }

    public void printSentencesByCountOfWords() {
        sentencesList.stream().sorted((Sentence s1, Sentence s2)
                -> s1.getWords().size() >= s2.getWords().size() ? 1 : -1)
                .forEach(sentence -> view.logger.info(sentence));
    }

    public void changeWordStartedFromVowelToLongestWord() {
        Pattern p = Pattern.compile("^[aeiouаоуеиіАОУЕИІ]+", Pattern.CASE_INSENSITIVE);
        String[][] array = sentencesToArray(sentencesList);
        for (int i = 0; i < array.length; i++) {
            String longest = Arrays.asList(array[i]).stream()
                    .max((word1, word2) -> word1.length() >= word2.length() ? 1 : -1).get();
            for (int j = 0; j < array[i].length; j++) {
                if (p.matcher(array[i][j]).find()) {
                    array[i][j] = longest;
                    break;
                }
            }
        }
        printArray(array);
    }

    public void sortWordsByPercentageOfVowel() {
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .sorted((String word1, String word2) ->
                        checkVowels(word1) >= checkVowels(word2) ? 1 : -1)
                .forEach(s -> view.logger.info(
                        s + " " + new DecimalFormat("#.00").format(checkVowels(s) * 100) + "%"));
    }

    public void printWordsInAscendingOrderByFirstCharacter() {
        List<String> list = Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .sorted()
                .collect(Collectors.toList());
        List<String> list1 = new LinkedList(list);
        list = list1;
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                System.out.println(list.get(i));
            } else {
                if (list.get(i).charAt(0) == list.get(i + 1).charAt(0)) {
                    System.out.print((list.get(i)) + ", ");
                } else {
                    System.out.println(list.get(i));
                }
            }
        }
    }

    public void printWordsStartsFromVowelOrderingByFirstConsonant() {
        StringBuffer stringBuffer = new StringBuffer();
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .filter((String x) -> checkIfStartsFromVowel(x) && checkFirstConsonant(x) > -1)
                .sorted((String s1, String s2) ->
                        Character.toString(s1.charAt(checkFirstConsonant(s1)))
                                .compareTo(Character.toString(s2.charAt(checkFirstConsonant(s2)))))
                .forEach(x -> stringBuffer.append(x).append(", "));
        if (stringBuffer.length() >= 2) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        view.logger.info(stringBuffer);
    }

    public void printWordsByCountOfSpecificCharacter(char character) {
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .sorted()
                .sorted((String word1, String word2) ->
                        countOfSpecChar(word1, character) >= countOfSpecChar(word2, character) ? 1 : -1)
                .forEach(s -> view.logger.info(
                        s + " " + countOfSpecChar(s, character)));
    }

    public void printWordsByCountOfSpecificCharacterDescending(char character) {
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .sorted(Comparator.reverseOrder())
                .sorted((String word1, String word2) ->
                        countOfSpecChar(word1, character) >= countOfSpecChar(word2, character) ? -1 : 1)
                .forEach(s -> view.logger.info(
                        s + " " + countOfSpecChar(s, character)));
    }

    public int countOfSpecChar(String word, char character) {
        return (word.length() - word.toLowerCase().replaceAll(
                Character.toString(character), "").length());
    }

    public void deleteWordsStartingFromConsonantWithSpecLength(int length) {
        StringBuffer stringBuffer = new StringBuffer();
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .filter(x -> !(!checkIfStartsFromVowel(x) && x.length() == length))
                .forEach(x -> view.logger.info(x));
    }

    public void findLongestPalindrome() {
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .filter(x -> x.equalsIgnoreCase(new StringBuffer(x).reverse().toString()))
                .sorted((x1, x2) -> x1.length() >= x2.length() ? -1 : 1)
                .limit(1)
                .forEach(x -> view.logger.info("The longest palindrome: " + x));
    }

    public void printWordsWithoutCharactersAsFirst() {
        Arrays.stream(sentencesToArray(sentencesList))
                .flatMap(Arrays::stream)
                .forEach(x -> view.logger.info(x.replaceAll(Character.toString(x.charAt(0)), "")));
    }

    public void replaceWordWithSpecLengthInSpecSentence(int numberOfSentence, int length, String word) {
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(sentencesToArray(sentencesList))
                .skip(numberOfSentence)
                .limit(1)
                .flatMap(Arrays::stream)
                .filter(x -> x.length() == length)
                .forEach(x -> view.logger.info(x + " -> " + word + ", "));
    }

    public void countOfOccurrencesOfWordFromList() {
        List<Word> list = new LinkedList();
        list.add(new Word("молодець"));
        list.add(new Word("і"));
        list.add(new Word("також"));
        view.logger.warn("Words to find: " + list);

        list.stream().forEach(word -> countOfOccurrencesOfWordInArray(word.getWord()));
    }

    private long countOfWordInSentence(String word, String[] array) {
        return Arrays.stream(array)
                .filter(wordi -> wordi.equals(word))
                .count();
    }

    private void countOfOccurrencesOfWordInArray(String word) {
        Arrays.stream(sentencesToArray(sentencesList))
                .filter(array -> countOfWordInSentence(word, array) > 0)
                .sorted((array1, array2) ->
                        countOfWordInSentence(word, array1) > countOfWordInSentence(word, array1) ? 1 : -1)
                .forEach( array -> view.logger.info("Array: " + arrayToString(array) + "   -> Word: " + word + " - "
                        + countOfWordInSentence(word, array)));
    }

    private String arrayToString(String[] array) {
        StringBuffer stringBuffer = new StringBuffer();
        Arrays.stream(array).forEach(s -> stringBuffer.append(s).append(", "));
        if (stringBuffer.length() >= 2) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        return stringBuffer.toString();
    }
}