package com.drohobytskyy.String.Model;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private List<Word> words;

    public Sentence() {
        this.words = new ArrayList<>();
    }

    public List<Word> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return "Sentence{" + words + '}';
    }
}
