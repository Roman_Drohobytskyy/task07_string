package com.drohobytskyy.String.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import com.drohobytskyy.String.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {
    public final static String EXIT = "exit";
    private BufferedReader input;
    public Controller controller;
    public Map<String, MenuItem> menu;
    public Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() throws IOException {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem(" 1   - Знайти речення з найбільшою кількістю повторюваних слів",
                controller::findSentenceWithMaxCountOfEqualWords));
        menu.put("2", new MenuItem(" 2   - Вивести всі речення заданого тексту у порядку зростання " +
                "кількості слів у кожному з них.",
                controller::printSentencesByCountOfWords));
        menu.put("3", new MenuItem(" 3   - Знайти таке слово у першому реченні, якого немає ні в " +
                "одному з інших речень.",
                controller::findUniqueWordInTextFromFirstSentence));
        menu.put("5", new MenuItem(" 5   - У кожному реченні тексту поміняти місцями перше слово, що " +
                "починається на голосну букву з найдовшим словом.",
                controller::changeWordStartedFromVowelToLongestWord));
        menu.put("6", new MenuItem(" 6   - Надрукувати слова тексту в алфавітному порядку по першій " +
                "букві. Слова, що починаються з нової букви, друкувати з абзацного відступу.",
                controller::printWordsInAscendingOrderByFirstCharacter));
        menu.put("7", new MenuItem(" 7   - Відсортувати слова тексту за зростанням відсотку голосних" +
                " букв співвідношення кількості голосних до загальної кількості букв у слові).",
                controller::sortWordsByPercentageOfVowel));
        menu.put("8", new MenuItem(" 8   - Слова тексту, що починаються з голосних букв, відсортувати " +
                "в алфавітному порядку по першій приголосній букві слова.",
                controller::printWordsStartsFromVowelOrderingByFirstConsonant));
        menu.put("9", new MenuItem(" 9   - Всі слова тексту відсортувати за зростанням кількості " +
                "заданої букви у слові (о). Слова з однаковою кількістю розмістити у алфавітному порядку.",
                controller::printWordsByCountOfSpecificCharacter));
        menu.put("10", new MenuItem(" 10   - Є текст і список слів. Для кожного слова з заданого списку " +
                "знайти, скільки разів воно зустрічається у кожному реченні, і відсортувати слова за спаданням " +
                "загальної кількості входжень.",
                controller::countOfOccurrencesOfWordFromList));
        menu.put("12", new MenuItem(" 12  - З тексту видалити всі слова заданої довжини (8), що " +
                "починаються на приголосну букву.",
                controller::deleteWordsStartingFromConsonantWithSpecLength));
        menu.put("13", new MenuItem(" 13  - Відсортувати слова у тексті за спаданням кількості " +
                "входжень заданого символу (о), а у випадку рівності – за алфавітом.",
                controller::printWordsByCountOfSpecificCharacterDescending));
        menu.put("14", new MenuItem(" 14  - У заданому тексті знайти підрядок максимальної довжини, " +
                "який є паліндромом, тобто, читається зліва на право і справа на ліво однаково.",
                controller::findLongestPalindrome));
        menu.put("15", new MenuItem(" 15  - Перетворити кожне слово у тексті, видаливши з нього всі " +
                "наступні входження першої букви цього слова.",
                controller::printWordsWithoutCharactersAsFirst));
        menu.put("16", new MenuItem(" 16  - У певному реченні тексту слова заданої довжини замінити " +
                "вказаним підрядком, довжина якого може не співпадати з довжиною слова. (1, 2, \"SURPRISE\")",
                controller::replaceWordWithSpecLengthInSpecSentence));
        menu.put(EXIT, new MenuItem(EXIT + " - exit from app", this::exitFromMenu));
    }

    public void executeMenu(Map<String, MenuItem> menu) throws IOException {
        String keyMenu;
        do {
            logger.warn("-------------------------------------------------------------------------------------"
                    + "-------------------------------------------------");

            logger.warn("Please, select menu point.");
            outputMenu(menu);
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu(Map<String, MenuItem> menu) {
        for (MenuItem pair : menu.values()) {
            logger.info(pair.getDescription());
        }
    }

    public void exitFromMenu() {
        logger.warn("Have an amazing day!");
        System.exit(0);
    }

}

