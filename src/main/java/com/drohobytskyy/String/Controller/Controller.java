package com.drohobytskyy.String.Controller;

import com.drohobytskyy.String.Model.MyFileReader;
import com.drohobytskyy.String.View.ConsoleView;

import java.io.IOException;

public class Controller {
    ConsoleView view;
    MyFileReader reader;

    public Controller(ConsoleView view, MyFileReader reader) throws IOException {
        this.view = view;
        this.reader = reader;
        view.controller = this;
        reader.view = view;
        reader.readerExecute();
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void printSentencesByCountOfWords() {
        reader.printAllSentences(reader.sentencesList);
        reader.printSentencesByCountOfWords();
    }

    public void findUniqueWordInTextFromFirstSentence() {
        reader.printAllSentences(reader.sentencesList);
        reader.findUniqueWordInTextFromFirstSentence();
    }

    public void changeWordStartedFromVowelToLongestWord() {
        reader.printAllSentences(reader.sentencesList);
        reader.changeWordStartedFromVowelToLongestWord();
    }

    public void printWordsInAscendingOrderByFirstCharacter() {
        reader.printAllSentences(reader.sentencesList);
        reader.printWordsInAscendingOrderByFirstCharacter();
    }

    public void sortWordsByPercentageOfVowel() {
        reader.printAllSentences(reader.sentencesList);
        reader.sortWordsByPercentageOfVowel();
    }

    public void printWordsStartsFromVowelOrderingByFirstConsonant() {
        reader.printAllSentences(reader.sentencesList);
        reader.printWordsStartsFromVowelOrderingByFirstConsonant();
    }

    public void printWordsByCountOfSpecificCharacter() {
        reader.printAllSentences(reader.sentencesList);
        reader.printWordsByCountOfSpecificCharacter('о');
    }

    public void deleteWordsStartingFromConsonantWithSpecLength() {
        reader.printAllSentences(reader.sentencesList);
        reader.deleteWordsStartingFromConsonantWithSpecLength(8);
    }

    public void printWordsByCountOfSpecificCharacterDescending() {
        reader.printAllSentences(reader.sentencesList);
        reader.printWordsByCountOfSpecificCharacterDescending('о');
    }

    public void findLongestPalindrome() {
        reader.printAllSentences(reader.sentencesList);
        reader.findLongestPalindrome();
    }

    public void printWordsWithoutCharactersAsFirst() {
        reader.printAllSentences(reader.sentencesList);
        reader.printWordsWithoutCharactersAsFirst();
    }

    public void replaceWordWithSpecLengthInSpecSentence() {
        reader.printAllSentences(reader.sentencesList);
        reader.replaceWordWithSpecLengthInSpecSentence(1, 2, "SURPRISE");
    }

    public void countOfOccurrencesOfWordFromList() {
        reader.printAllSentences(reader.sentencesList);
        reader.countOfOccurrencesOfWordFromList();
    }

    public void findSentenceWithMaxCountOfEqualWords() {
        reader.printAllSentences(reader.sentencesList);
        reader.findSentenceWithMaxCountOfEqualWords();
    }
}
